```
   _____                                __________                                                   .__                
  /  _  \   _________.__. ____   ____   \______   \_______  ____   ________________    _____   _____ |__| ____    ____  
 /  /_\  \ /  ___<   |  |/    \_/ ___\   |     ___/\_  __ \/  _ \ / ___\_  __ \__  \  /     \ /     \|  |/    \  / ___\ 
/    |    \\___ \ \___  |   |  \  \___   |    |     |  | \(  <_> ) /_/  >  | \// __ \|  Y Y  \  Y Y  \  |   |  \/ /_/  >
\____|__  /____  >/ ____|___|  /\___  >  |____|     |__|   \____/\___  /|__|  (____  /__|_|  /__|_|  /__|___|  /\___  / 
        \/     \/ \/         \/     \/                          /_____/            \/      \/      \/        \//_____/  
```

\- *by: Faiz Shukri*

## Run

There are 2 project here. Golang and nodejs.

For golang project, navigate into golang directory, and run below command:

```sh
go run *.go <number>
```

For nodejs project, navigate into nodejs directory, and run below command:

```sh
node main.js <number>
```


`<number>` is the number in the filename prefix inside each folder. (eg: `1`, `3`, `3b` etc)