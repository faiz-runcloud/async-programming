package main

import (
	"fmt"
	"time"
)

// Synchronous
func Sync() {
	fmt.Println("1. Synchronous\n")

	for i := 1; i <= 5; i++ {
		HeavyFunc(Option{Value: i, TimeTakenDuration: 200 * time.Millisecond, PrintValue: true})
	}
}
