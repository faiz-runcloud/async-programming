package main

import (
	"fmt"
	"time"
)

// Fire and forget
func BasicAsync() {
	fmt.Println("2. Basic Async (Fire and forget)\n")

	for i := 1; i <= 5; i++ {
		go HeavyFunc(Option{Value: i, TimeTakenDuration: 200 * time.Millisecond, PrintValue: true})
	}
}
