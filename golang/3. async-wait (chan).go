package main

import (
	"fmt"
	"time"
)

// Wait until all tasks completed
func AsyncWait() {
	fmt.Println("3. Waiting tasks to completed (Using Channel)\n")

	done := make(chan bool)

	// Wait one task
	fmt.Println("Wait one task")
	go func() {
		HeavyFunc(Option{Value: 1, TimeTakenDuration: 200 * time.Millisecond, PrintValue: true})
		done <- true
	}()
	<-done

	// Wait multiple tasks
	fmt.Println("\nWait multiple tasks")
	for i := 2; i <= 5; i++ {
		i := i // https://golang.org/doc/faq#closures_and_goroutines
		go func() {
			HeavyFunc(Option{Value: i, TimeTakenDuration: 200 * time.Millisecond, PrintValue: true})
			done <- true
		}()
	}

	for i := 2; i <= 5; i++ {
		<-done
	}
}
