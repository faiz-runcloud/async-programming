package main

import (
	"fmt"
	"sync"
	"time"
)

// Wait until all tasks completed
func AsyncWaitWg() {
	fmt.Println("3b. Waiting tasks to completed (Using WaitGroup)\n")

	var wg sync.WaitGroup

	// Wait one task
	fmt.Println("Wait one task")
	wg.Add(1)
	go func() {
		defer wg.Done()
		HeavyFunc(Option{Value: 1, TimeTakenDuration: 200 * time.Millisecond, PrintValue: true})
	}()
	wg.Wait()

	// Wait multiple tasks
	fmt.Println("\nWait multiple tasks")
	wg.Add(4)
	for i := 2; i <= 5; i++ {
		i := i // https://golang.org/doc/faq#closures_and_goroutines
		go func() {
			defer wg.Done()
			HeavyFunc(Option{Value: i, TimeTakenDuration: 200 * time.Millisecond, PrintValue: true})
		}()
	}
	wg.Wait()
}
