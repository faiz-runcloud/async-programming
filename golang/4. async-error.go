package main

import (
	"fmt"
	"time"

	"golang.org/x/sync/errgroup"
)

// Wait until all tasks completed,
// or until the first error occur.
//
// If the error occur, get the error.
func AsyncError() {
	fmt.Println("4. Waiting tasks to completed and watch for error\n")

	var eg errgroup.Group

	for i := 1; i <= 5; i++ {
		i := i // https://golang.org/doc/faq#closures_and_goroutines
		eg.Go(func() error {
			timeTaken := 200 * time.Millisecond
			shouldReturnError := false

			if i == 2 {
				timeTaken = 100 * time.Millisecond // to simulate an error occurs way earlier
				shouldReturnError = true
			}

			if _, err := HeavyFunc(Option{Value: i, TimeTakenDuration: timeTaken, PrintValue: true, ReturnError: shouldReturnError}); err != nil {
				return err
			}

			return nil
		})
	}

	if err := eg.Wait(); err != nil {
		fmt.Println(err)
	}
}
