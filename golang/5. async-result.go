package main

import (
	"fmt"
	"sync"
	"time"
)

// Wait until all tasks completed,
// and get the output of all tasks.
func AsyncResult() {
	fmt.Println("5. Waiting tasks to completed and get it's output\n")

	var wg sync.WaitGroup

	var results = make([]int, 5)
	resultCh := make(chan int)

	wg.Add(5)
	for i := 1; i <= 5; i++ {
		i := i // https://golang.org/doc/faq#closures_and_goroutines
		go func() {
			defer wg.Done()

			res, _ := HeavyFunc(Option{Value: i, TimeTakenDuration: 200 * time.Millisecond, PrintValue: true})

			resultCh <- res
		}()
	}

	go func() {
		for i := range results {
			results[i] = <-resultCh
		}
	}()

	wg.Wait()
	close(resultCh)

	fmt.Println("Results:", results)
}
