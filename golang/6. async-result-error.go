package main

import (
	"fmt"
	"time"

	"golang.org/x/sync/errgroup"
)

// Wait until all tasks completed,
// or until the first error occur.
//
// If the error occur, get the error,
// else, get the output of all tasks.
func AsyncResultAndError() {
	fmt.Println("6. Waiting tasks to completed, watch for error and get it's output\n")

	var eg errgroup.Group

	var results = make([]int, 5)
	resultCh := make(chan int)

	for i := 1; i <= 5; i++ {
		i := i // https://golang.org/doc/faq#closures_and_goroutines
		eg.Go(func() error {
			timeTaken := 200 * time.Millisecond
			shouldReturnError := false

			if i == 2 {
				timeTaken = 100 * time.Millisecond // to simulate an error occurs way earlier
				shouldReturnError = true
			}
			res, err := HeavyFunc(Option{Value: i, TimeTakenDuration: timeTaken, PrintValue: true, ReturnError: shouldReturnError})

			if err != nil {
				return err
			}

			resultCh <- res
			return nil
		})
	}

	go func() {
		for i := range results {
			results[i] = <-resultCh
		}
	}()

	if err := eg.Wait(); err != nil {
		fmt.Println(err)
	}

	close(resultCh)

	fmt.Println("Results:", results)
}
