package main

import (
	"fmt"
	"time"
)

func Duration(f func()) {
	start := time.Now()
	f()
	t := time.Now()
	elapsed := t.Sub(start)

	fmt.Printf("\nCompleted in: %dms\n===\n\n", int64(elapsed/time.Millisecond))
}

type Option struct {
	Value             int
	TimeTakenDuration time.Duration
	PrintValue        bool
	ReturnError       bool
}

func HeavyFunc(option Option) (int, error) {
	time.Sleep(option.TimeTakenDuration)

	if option.PrintValue {
		fmt.Printf("[%v] => %d\n", option.TimeTakenDuration, option.Value)
	}

	if option.ReturnError {
		return 0, fmt.Errorf("Fake error occurs for value %d", option.Value)
	}

	return option.Value, nil
}
