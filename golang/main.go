package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Please provide a number.")
		os.Exit(1)
	}

	arg := os.Args[1]

	switch arg {
	case "1":
		Duration(Sync)
	case "2":
		Duration(BasicAsync)
	case "3":
		Duration(AsyncWait)
	case "3b":
		Duration(AsyncWaitWg)
	case "4":
		Duration(AsyncError)
	case "5":
		Duration(AsyncResult)
	case "6":
		Duration(AsyncResultAndError)
	}

	time.Sleep(300 * time.Millisecond)
}
