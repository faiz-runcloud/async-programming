const { heavyFunc } = require("./helper");

// Synchronous
module.exports = () => {
  console.log("1. Synchronous\n");

  for (let i = 1; i <= 5; i++) {
    heavyFunc({ value: i, timeTakenMs: 200 });
  }
};
