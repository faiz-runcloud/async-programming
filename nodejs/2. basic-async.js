const { heavyFuncAsync } = require("./helper");

// Fire and forget
module.exports = () => {
  console.log("2. Basic Async (Fire and forget)\n");

  for (let i = 1; i <= 5; i++) {
    heavyFuncAsync({ value: i, timeTakenMs: 200 });
  }
};
