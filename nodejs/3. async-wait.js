const { heavyFuncAsync } = require("./helper");

// Wait until all tasks completed
module.exports = async () => {
  console.log("3. Waiting tasks to completed\n");

  // Wait one task
  console.log("Wait one task");
  await heavyFuncAsync({ value: 1, timeTakenMs: 200 });

  // Wait multiple tasks
  console.log("\nWait multiple task");
  const promises = [];
  for (let i = 2; i <= 5; i++) {
    const promise = heavyFuncAsync({ value: i, timeTakenMs: 200 });

    promises.push(promise);
  }
  await Promise.all(promises);
};
