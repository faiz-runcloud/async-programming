const { heavyFuncAsync } = require("./helper");

// Wait until all tasks completed,
// or until the first error occur.
//
// If the error occur, get the error.
module.exports = async () => {
  console.log("4. Waiting tasks to completed and watch for error\n");

  const promises = [];

  for (let i = 1; i <= 5; i++) {
    const promise = heavyFuncAsync({
      value: i,
      timeTakenMs: i == 2 ? 100 : 200, // to simulate an error occurs way earlier
      returnError: i == 2,
    });

    promises.push(promise);
  }

  await Promise.all(promises).catch((err) => {
    // Handle error here
    console.error(err);
  });

  /*
  // Alternatively, we can use try / catch as well

  try {
    await Promise.all(promises)
  } catch (err) {
    console.error(err);
  }
  */
};
