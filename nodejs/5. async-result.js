const { heavyFuncAsync } = require("./helper");

// Wait until all tasks completed,
// and get the output of all tasks.
module.exports = async () => {
  console.log("5. Waiting tasks to completed and get it's output\n");

  const promises = [];

  for (let i = 1; i <= 5; i++) {
    const promise = heavyFuncAsync({
      value: i,
      timeTakenMs: 200,
    });

    promises.push(promise);
  }

  const results = await Promise.all(promises);

  console.log("Results:", results);
};
