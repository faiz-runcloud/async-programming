const { heavyFuncAsync } = require("./helper");

// Wait until all tasks completed,
// or until the first error occur.
//
// If the error occur, get the error,
// else, get the output of all tasks.
module.exports = async () => {
  console.log(
    "6. Waiting tasks to completed, watch for error and get it's output\n"
  );

  const promises = [];

  for (let i = 1; i <= 5; i++) {
    const promise = heavyFuncAsync({
      value: i,
      timeTakenMs: i == 2 ? 100 : 200, // to simulate an error occurs way earlier
      returnError: i == 2,
    });

    promises.push(promise);
  }

  const results = await Promise.all(promises).catch((err) => {
    // Handle error here
    console.error(err);

    // We can return alternative results too. By default its undefined.
    // return []
  });

  console.log("Results:", results);
};
