const duration = async (f) => {
  var date1 = new Date();

  await f();
  var date2 = new Date();
  console.log(`\nCompleted in: ${date2 - date1}ms\n===\n`);
};

// Non-async sleep (blocking)
const heavyFunc = (options = {}) => {
  options = Object.assign(
    {},
    {
      value: 0,
      timeTakenMs: 0,
      printValue: true,
      returnError: false,
    },
    options
  );

  var date = new Date();
  var curDate = null;
  do {
    curDate = new Date();
  } while (curDate - date < options.timeTakenMs);

  if (options.printValue) {
    console.log(`[${options.timeTakenMs}ms] =>`, options.value);
  }

  if (options.returnError) {
    throw new Error(`Fake error occurs for value ${options.value}`);
  }

  return options.value;
};

const heavyFuncAsync = async (options = {}) => {
  options = Object.assign(
    {},
    {
      value: 0,
      timeTakenMs: 0,
      printValue: true,
      returnError: false,
    },
    options
  );

  return new Promise((resolve, reject) =>
    setTimeout(() => {
      if (options.printValue) {
        console.log(`[${options.timeTakenMs}ms] =>`, options.value);
      }

      if (options.returnError) {
        return reject(
          new Error(`Fake error occurs for value ${options.value}`)
        );
      }

      resolve(options.value);
    }, options.timeTakenMs)
  );
};

module.exports = {
  duration,
  heavyFunc,
  heavyFuncAsync,
};
