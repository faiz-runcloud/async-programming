const { duration } = require("./helper");
const arg = process.argv[2];

if (!arg) {
  console.error("Please provide a number.");
  process.exit();
}

switch (arg) {
  case "1":
    duration(require("./1. sync"));
    break;

  case "2":
    duration(require("./2. basic-async"));
    break;

  case "3":
    duration(require("./3. async-wait"));
    break;

  case "4":
    duration(require("./4. async-error"));
    break;

  case "5":
    duration(require("./5. async-result"));
    break;

  case "6":
    duration(require("./6. async-result-error"));
    break;
}
